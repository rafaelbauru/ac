require 'test_helper'
include Devise::TestHelpers

class SubtasksControllerTest < ActionController::TestCase
  setup do
    @subtask = subtasks(:one)
  end

  test "should get index" do
    sign_in users(:rafael)

    get :index
    assert_response :success
    assert_not_nil assigns(:subtasks)
  end

  test "should get new" do
    sign_in users(:rafael)

    get :new
    assert_response :success
  end

  test "should create subtask" do
    sign_in users(:rafael)
    assert_difference('Subtask.count') do
      post :create, subtask: { description: @subtask.description, task_id: @subtask.task_id }
    end

    #assert_redirected_to subtask_path(assigns(:subtask))
  end

  test "should show subtask" do
    sign_in users(:rafael)

    get :show, id: @subtask
    assert_response :success
  end

  test "should get edit" do
    sign_in users(:rafael)

    get :edit, id: @subtask
    assert_response :success
  end

  test "should update subtask" do
    sign_in users(:rafael)

    patch :update, id: @subtask, subtask: { description: @subtask.description, task_id: @subtask.task_id }
    assert_redirected_to subtask_path(assigns(:subtask))
  end

#   test "should destroy subtask" do
#     assert_difference('Subtask.count', -1) do
#       delete :destroy, id: @subtask
#     end
#
#     assert_redirected_to subtasks_path
#   end
 end
