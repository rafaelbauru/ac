require 'test_helper'
include Devise::TestHelpers

class TasksControllerTest < ActionController::TestCase
   setup do
     @task = tasks(:one)
   end

   test "should get index" do
     #@request.env["devise.mapping"] = Devise.mappings[:user]
     sign_in users(:rafael)

     get :index
     assert_response :success
     assert_not_nil assigns(:tasks)
   end

  test "should get new" do
    sign_in users(:rafael)

    get :new
    assert_response :success
  end

  test "should create task" do
    sign_in users(:rafael)

    assert_difference('Task.count') do
      post :create, task: { description: @task.description, public: @task.public, user_id: @task.user_id }
    end

    # assert_redirected_to task_path(assigns(:task))
  end

  test "should show task" do
    sign_in users(:rafael)
    get :show, id: @task
    assert_response :success
  end

  test "should get edit" do
    sign_in users(:rafael)
    get :edit, id: @task
    assert_response :success
  end

  test "should update task" do
    sign_in users(:rafael)

    patch :update, id: @task, task: { description: @task.description, public: @task.public, user_id: @task.user_id }
    # assert_redirected_to tasks_path
  end

  test "should destroy task" do
    sign_in users(:rafael)

    assert_difference('Task.count', -1) do
      delete :destroy, id: @task
    end

    assert_redirected_to tasks_path
  end

  # test "update product via javascript" do
  #   # use xhr to simulate a ajax call
  #   xhr :get, :product_update, format: :js, product_id: '77'
  #   # if the controller responds with a javascript file the response will be a success
  #   assert_response :success
  # end



end
