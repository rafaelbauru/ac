# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Task.create(user_id: 1, description: Array.new(18){[*'0'..'9', *'a'..'z', *'A'..'Z'].sample}.join, public: true)
Task.create(user_id: 1, description: Array.new(22){[*'0'..'9', *'a'..'z', *'A'..'Z'].sample}.join, public: true)
Task.create(user_id: 1, description: Array.new(15){[*'0'..'9', *'a'..'z', *'A'..'Z'].sample}.join, public: false)

Task.create(user_id: 2, description: Array.new(22){[*'0'..'9', *'a'..'z', *'A'..'Z'].sample}.join, public: true)
Task.create(user_id: 2, description: Array.new(18){[*'0'..'9', *'a'..'z', *'A'..'Z'].sample}.join, public: true)
Task.create(user_id: 2, description: Array.new(25){[*'0'..'9', *'a'..'z', *'A'..'Z'].sample}.join, public: false)

Subtask.create(description: Array.new(25){[*'0'..'9', *'a'..'z', *'A'..'Z'].sample}.join, task_id: '1' )
Subtask.create(description: Array.new(25){[*'0'..'9', *'a'..'z', *'A'..'Z'].sample}.join, task_id: '1' )
Subtask.create(description: Array.new(25){[*'0'..'9', *'a'..'z', *'A'..'Z'].sample}.join, task_id: '1' )

Subtask.create(description: Array.new(25){[*'0'..'9', *'a'..'z', *'A'..'Z'].sample}.join, task_id: '2' )
Subtask.create(description: Array.new(25){[*'0'..'9', *'a'..'z', *'A'..'Z'].sample}.join, task_id: '2' )
Subtask.create(description: Array.new(25){[*'0'..'9', *'a'..'z', *'A'..'Z'].sample}.join, task_id: '2' )