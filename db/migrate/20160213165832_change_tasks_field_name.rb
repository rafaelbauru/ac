class ChangeTasksFieldName < ActiveRecord::Migration
  def change
    rename_column :tasks, :user_owner_id, :user_id
    change_column_null :tasks, :description, :user_id, false
    change_column_null :subtasks, :task_id, false
  end
end
