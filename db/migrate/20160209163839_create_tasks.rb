class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :description
      t.integer :father_task_id
      t.integer :user_owner_id
      t.boolean :public

      t.timestamps null: false
    end
  end
end
