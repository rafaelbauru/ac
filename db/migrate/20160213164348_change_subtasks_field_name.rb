class ChangeSubtasksFieldName < ActiveRecord::Migration
  def change
    rename_column :subtasks, :task_father_id, :task_id
    change_column_null :subtasks, :description, false
  end
end
