class CreateSubtasks < ActiveRecord::Migration
  def change
    create_table :subtasks do |t|
      t.string :description
      t.integer :task_father_id

      t.timestamps null: false
    end
  end
end
