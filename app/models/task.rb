class Task < ActiveRecord::Base
  validates :description,:user_id,  presence: true
  has_many :subtask, dependent: :destroy

end
