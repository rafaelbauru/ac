class Subtask < ActiveRecord::Base
  validates :description, :task_id, presence: true
  #has_one :task
  belongs_to :task

end
