class SubtasksController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_subtask, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @subtasks = Subtask.all
   # respond_with(@subtasks)
    respond_to do |format|
      format.html { @subtasks }
      format.json { render json: @subtasks}

    end
  end

  def show
    #respond_with(@subtask)
    respond_to do |format|
      format.html { @subtasks }
      format.json { render json: @subtasks }
    end
  end

  def new
    @subtask = Subtask.new
     respond_with(@subtask)
  end

  def create
    @subtask = Subtask.new(subtask_params)
    #@subtask.save
    #respond_with(@subtask)

    respond_to do |format|
      if @subtask.save
        format.html { redirect_to tasks_path, notice: 'Subtask was successfully created.' }
        #format.html { redirect_to @task, notice: 'Task was successfully created.' }
        format.json { render :show, status: :created, location: @subtask }
        format.js {@subtask}
      else
        format.html { render :new }
        format.json { render json: @subtask.errors, status: :unprocessable_entity }
        format.js
      end
    end

  end

  def update
    @subtask.update(subtask_params)
    respond_with(@subtask)
  end

  def destroy
    @subtask.destroy
    #respond_with(@subtask)

    respond_to do |format|
      format.html { redirect_to tasks_url, notice: 'Subtask was successfully destroyed.' }
      format.json { head :no_content }
      format.js
    end

  end

  private
    def set_subtask
      @subtask = Subtask.find(params[:id])
    end

    def subtask_params
      params.require(:subtask).permit(:description, :task_id)
    end
end
