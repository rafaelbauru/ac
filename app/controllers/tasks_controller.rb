class TasksController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_task, only: [:show, :edit, :update, :destroy]

  # GET /tasks
  # GET /tasks.json
  def index

    @tasks = Task.where(user_id: current_user.id)+(Task.where( public: true))
    @tasks.uniq!
    @subtask = Subtask.new
    @subtasks = Subtask.all

  end

  # GET /tasks/1
  # GET /tasks/1.json
  def show
  end

  # GET /tasks/new
  def new
    @task = Task.new
  end

  # GET /tasks/1/edit
  def edit
  end

  # POST /tasks
  # POST /tasks.json
  def create
    @task = Task.new(task_params)
    @task.user_id= current_user.id

    if not @task.public?
      @task.public = false
    else
      @task.public = true
    end

    respond_to do |format|
      if @task.save
        format.html { redirect_to tasks_path, notice: 'Task was successfully created.' }
        #format.html { redirect_to @task, notice: 'Task was successfully created.' }
        format.json { render :show, status: :created, location: @task }
      else
        format.html { render :new }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tasks/1
  # PATCH/PUT /tasks/1.json
  def update

    respond_to do |format|
      if @task.update(task_params) and has_permission?
        format.html { redirect_to tasks_path, notice: 'Task was successfully updated.' }
        format.json { render :show, status: :ok, location: @task }
      else
        flash[:error] = "You don't have permission to access" if not has_permission?

        format.html { render :edit }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end

    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    if current_user.id == @task.user_id
      @task.destroy

      #TODO remove subtasks

      respond_to do |format|
        format.html { redirect_to tasks_url, notice: 'Task was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      flash[:error] = "You don't have permission to access"
      redirect_to tasks_url
    end
  end

  # HELPERS

  helper_method :substask_get, :has_permission?

  def has_permission?
    true if current_user.id == @task.user_id
  end

  def substask_get(id)
    @subtasks= Subtask.where(task_id: id)
    if @subtasks
      @subtasks
    else
      @subtasks= {}
    end
  end

  def has_permission?
    true if current_user.id == @task.user_id
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task
      @task = Task.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def task_params
      params.require(:task).permit(:description, :public, :id_user)
    end
end
